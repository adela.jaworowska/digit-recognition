function [sepplane fp fn] = perceptron(pclass, nclass, epochs=200)
% Computes separating plane (linear classifier) using
% perceptron method.
% pclass - 'positive' class (one row contains one sample)
% nclass - 'negative' class (one row contains one sample)
% Output:
% sepplane - row vector of separating plane coefficients
% fp - false positive count (i.e. number of misclassified samples of pclass)
% fn - false negative count (i.e. number of misclassified samples of nclass)

  sepplane = rand(1, columns(pclass) + 1) - 0.5;
  tset = [ ones(rows(pclass), 1) pclass; -ones(rows(nclass), 1) -nclass];
  nPos = rows(pclass); % number of positive samples
  nNeg = rows(nclass); % number of negative samples
  min_change = 0.001;
  
  error = 0;
  
  i = 1;
  do 
		alpha = 0.001/i;		
		res = tset * sepplane';

		error = sum(tset(res<0, :)); 
		
		d_sepplane = alpha * error;
		sepplane += d_sepplane;
	
		error = 0;
		++i;
		%if norm(d_sepplane) < min_change
		%	break;
		%end
  until i >= epochs;
    
  % Compute the numbers of false positives and false negatives 
  res = tset * sepplane';

  fp = sum(res(1:nPos, 1) < 0);
  fn = sum(res(nPos + 1 : nPos + nNeg, 1) < 0);
