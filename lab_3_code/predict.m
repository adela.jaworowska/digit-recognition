function [predicted] = predict(data, weights)

	% multiply data by weights and add bias 
    activation = [weights(2:end)] * transpose(data(2:end)) + weights(1);
	
    if activation >= 0
        predicted = 1;
    else 
        predicted = -1;
    end
end
